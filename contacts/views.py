import json
from django.http import JsonResponse
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from contacts.models import ContactsMongoOperations, ContactMissing

contacts_operations = ContactsMongoOperations()


class ContactsView(View):

    def get(self, request, phone, *args, **kwargs):
        contact_phone = phone
        try:
            contact = contacts_operations.find(contact_phone)
        except ContactMissing:
            return JsonResponse({"found": False, "message": "Contact {} not found".format(contact_phone)})

        return JsonResponse({"found": True, "name": contact["name"]})

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        try:
            request_json = json.loads(request.body)
        except ValueError:
            return JsonResponse({"success": False, "messagE": "Bad JSON input"})

        is_doc_new = contacts_operations.upsert(
            request_json.get("phone", None),
            request_json.get("name", None),
        )
        response_json = {
            "success": True,
            "new": is_doc_new,
        }
        return JsonResponse(response_json)

    def delete(self, request, phone, *args, **kwargs):
        return JsonResponse({"msg": "this is contacts DELETE"})

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ContactsView, self).dispatch(request, *args, **kwargs)
