from django.db import models
from django_test.mongo import MongoConn


mongo = MongoConn.client()


class ContactMissing(BaseException):
    def __init__(self, phone=None):
        super(BaseException, self).__init__("Contact with phone {} is missing".format(phone))


class ContactsMongoOperations():

    def __init__(self, collection="contacts"):
        self.collection = mongo.get_collection("contacts")

    def upsert(self, phone, name):
        """Returns True if it's a new document, False if it's an update"""
        document_search = {"_id": phone}  # search by this
        contact_document = dict(document_search)  # update by this
        contact_document.update({"name": name})
        document = self.collection.replace_one(document_search, contact_document, upsert=True)
        return True if document.upserted_id else False

    def find(self, phone):
        import pdb; pdb.set_trace()
        find_result = self.collection.find_one({"_id": phone})
        if find_result is None:
            raise ContactMissing(phone)
        else:
            return find_result

    def delete(self, _id):
        pass
