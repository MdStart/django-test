# Django test

This is a shell for a Django interview test, allowing interviewers give the interviewee various tasks within the framework to test their abilities.

## Interviewer

Most of the times, interviewees can't draw concepts and buzz-words in an interview, but giving an assignment can provide real insight in the person's amount of knowledge and ability to learn.

## Interviewee

This test should allow you to show your knowledge as well your ability to learn new stuff, follow the interviewer's instruction carefully, details matter.

### How to use

The test is currently basing the first step on Ubuntu and derivative OSs, more may be added in the future.
All other steps are relevant for all OSs.

##### 1. Install required programs

If you have the knowledge to do it your self, great, else, run the file `os_requirements.sh`.

*Notice: In OSs like Linux Mint running the script isn't recommended, because they don't show the upstream Ubuntu version.*

* python v2.7 - should already be installed on your OS, (Check by running `python --version` in the shell)
* docker - used for running applications without the need to disturb your regular apps installed
    * Based on the manual here: https://docs.docker.com/engine/installation/linux/ubuntu/

##### 2. Install python packages

* Global:
    * virtualenv / virtualenvwrapper / pipfile / pipenv - your choice
* Project:
    * Django - Well...
    * docker-compose - To bring up the docker containers quickly
    * redis_cache - Utilize Django's cache module with redis
    * pymongo - A good mongo python library
    * MySQL-python - Relational DB

##### 3. Start the required docker containers

* run `docker-compose up -d`
* Currently we start mysql (MariaDB), Mongo and Redis

##### 4. Start coding

##### 5. Send back the completed test in the manner described in the instructions

##### 6. Remember to take down the containers running `docker-compose down -v --rmi all` to save the space

##### 7. Get hired

### Extra info

#### Redis

Redis can be accessed via Django's builtin cache mechanism, it is already setup, so simply follow the manual here: [Django cache topic#low level api](https://docs.djangoproject.com/en/1.8/topics/cache/#the-low-level-cache-api).

#### MongoDB

MongoDB is setup using pyMongo with a custom API access for simplicity, to use it add follow this code piece:

```python
from django_test.mongo import MongoConn

mongo = MongoConn.client()
mongo.db.collection_name.find({"field": "value"})
```

Notice `collection_name` is the specific collection you need, so replace it with whatever you need.
* `mongo.db` returns the Database object, as described in [pymongo database](https://api.mongodb.com/python/current/api/pymongo/database.html).
* `mongo.db.collection_name` returns the Collection object, as described in [pymongo collection](https://api.mongodb.com/python/current/api/pymongo/collection.html).

## License

MIT, 2017 Sync.ME Ltd. see [LICENSE](LICENSE)
