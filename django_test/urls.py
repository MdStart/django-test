"""django_test URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from pages.views import homepage
from contacts.views import ContactsView

urlpatterns = [
    url(r'^/?$', homepage, name="homepage"),
    url(r'^contacts/(?P<phone>[+0-9\-]+)?', ContactsView.as_view(), name="contacts"),
    url(r'^admin/', include(admin.site.urls)),
]
