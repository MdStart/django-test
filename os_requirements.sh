#!/bin/bash

if [ "$(id -u)" = "0" ]; then
    echo 'Do not run this as root, the script uses sudo when needed'
    exit 1
fi

if hash python 2>/dev/null; then
    echo 'Got python :), we can continue'
else
    echo 'You need python to continue :('
    exit 1
fi

if hash docker 2>/dev/null; then
    echo 'Got docker, skipping'
else
    echo 'Installing docker'
    sudo apt-get update && apt-get install -y \
        apt-transport-https \
        ca-certificates \
        curl \
        software-properties-common
    sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    sudo apt-get update && apt-get install docker-ce
    sudo usermod -aG docker ${USER}
fi

